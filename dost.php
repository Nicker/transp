<?php
abstract class BaseTransport {
    public float $price;
    public string $date;
    public string $error;

    public function __construct (string $err  ) {
        $this->error = $err;
    }
    
    function toArray (){

        return array(
                "error" => $this->error,
                "date" => $this->date,
                "price" => $this->price
        );
    }

    function toJSON (){
        return json_encode($this->toArray());
    }
 }

 class SlowTransport extends BaseTransport {
     private float $basePrice = 150;

     function __construct (array $return) {
        parent::__construct($return["error"]);
        $this->price = $return["coefficient"] * $this->basePrice;
        $this->date = $return["date"];
     }
 }

 class FastTransport extends BaseTransport {
     
    function __construct (array $return) {
       parent::__construct($return["error"]);
       $this->date =  date("Y-m-d", strtotime("+" . $return["period"] . " days"));
       $this->price = $return["price"];
    }
}

class TransportFactory {
    function create (string $input) {
        $transport=json_decode($input, true);
        if (array_key_exists("period", $transport)) {
            return new FastTransport($transport);  
        }
        else if (array_key_exists("coefficient", $transport)) {
            return new SlowTransport($transport);  
        }
        else { 
            throw new Exception("Транспорт не определен!");

        }
    }
}
 
?>