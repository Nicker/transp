<?php

class Emul {

    public string $source;
    public string $target;
    public float $weight;
    public float $price;
    public float $coefficient;
    public string $date;
    public int $period;


    function __construct (string $json) {
        $arr = json_decode($json, true);
        $this->source = $arr["source"];
        $this->target = $arr["target"];
        $this->weight = $arr["weight"];
        $this->price = mt_rand(200, 300) * $this->weight / 2.0;
        $this->period = mt_rand(2, 5);
        $this->date = date("Y-m-d", strtotime("+".$this->period." days"));
        $this->coefficient = $this->price / mt_rand(140, 160);
    }

    public function fast () {
        return json_encode(array(
            "price" => $this->price,
            "period" => $this->period,
            "error" => "OK"
        ));
    }
    public function slow () {
        return json_encode(array(
            "coefficient" => $this->coefficient,
            "date" => $this->date,
            "error" => "OK"
        ));
    }
}
?>